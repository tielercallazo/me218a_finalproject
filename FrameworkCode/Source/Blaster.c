/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Blaster.h"
#include "InitializeHardware.h"
#include "Game.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static BlasterState_t CurrentState;
static uint8_t lastButton;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

bool CheckBlasterButtonStatus(void);

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitBlasterFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = BlasterInit;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostBlaster(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunBlaster(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  // printf("\r\n%u", CurrentState);

  switch (CurrentState)
  {
    case BlasterInit:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // activate blaster HW
        CurrentState = BlasterDeactive; 
      } else if(ThisEvent.EventType == ES_COIN_DETECTED) {
//        CurrentState = BlasterReady2Fire;
//        lastButton = ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI);
			}
    }
    break;
    
    case BlasterDeactive:
    {
      if(ThisEvent.EventType == ES_COIN_DETECTED) {
        // start the game
        lastButton = ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI);
        CurrentState = BlasterReady2Fire;
      }
    }
    break;

    case BlasterReady2Fire:
    {
			if(ThisEvent.EventType == ES_BLASTER_BUTTON_DOWN) {
				// reset the timer
				ES_Timer_InitTimer(BLASTER_TIMER, 250);
				
				// pulse the gun
				PWM_TIVA_SetDuty(50, BLASTER_IR_CHANNEL); // 100HZ in HW init
				
				// turn on the motor
				HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) |= BLASTER_MOTOR_HI;
        
        ScorePenaltyBlaster();
				
				// restart the inactivity timer
				ES_Timer_InitTimer(INACTIVITY_TIMER, 30000);
				
				// change states
				CurrentState = BlasterFiring;
			} else if(ThisEvent.EventType == ES_GAME_OVER) {
        CurrentState = BlasterDeactive;
			}
		}
		break;
			
		case BlasterFiring:
		{
      
			if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BLASTER_TIMER) {
				// stop the motor
				HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) &= BLASTER_MOTOR_LO;
				
				// stop the pulsing
				PWM_TIVA_SetDuty(0, BLASTER_IR_CHANNEL);
				
				// transition state
				CurrentState = BlasterCoolDown;
			} else if(ThisEvent.EventType == ES_GAME_OVER) {
        // turn off the blaster 
        PWM_TIVA_SetDuty(0, BLASTER_IR_CHANNEL);
        // move to deactive state
        CurrentState = BlasterDeactive;
			}
		}
		break;
			
		case BlasterCoolDown:
		{
			if(ThisEvent.EventType == ES_BLASTER_BUTTON_UP) {
				// restart the 50ms debounce timer
				ES_Timer_InitTimer(BLASTER_DEBOUNCE_TIMER, 50);
				
				CurrentState = BlasterDebounce;
			} else if(ThisEvent.EventType == ES_GAME_OVER) {
        CurrentState = BlasterDeactive;
			}
		}
		break;
		
		case BlasterDebounce:
		{
			if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BLASTER_DEBOUNCE_TIMER) {
				CurrentState = BlasterReady2Fire;
			} else if(ThisEvent.EventType == ES_GAME_OVER) {
        CurrentState = BlasterDeactive;
			}
		}
		break;
			
		default:
			;
	}
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
BlasterState_t QueryBlasterFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

bool CheckBlasterButtonStatus(void) {
  // read the current button
  static uint8_t currentButton;
  currentButton = ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI);
  
  if(lastButton != currentButton) {
    if(currentButton == 0) {
      // button low
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BLASTER_BUTTON_DOWN;
      ThisEvent.EventParam = 1;
      // printf("\n\rbutt down");
      PostBlaster(ThisEvent);
    } else {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BLASTER_BUTTON_UP;
      ThisEvent.EventParam = 1;
      // printf("\n\rbutt up");
      PostBlaster(ThisEvent);
    }
    lastButton = currentButton;
    return true;
  }
  return false;
}
  
