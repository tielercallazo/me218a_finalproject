/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "AlienFSM_2.h"
#include "TestCases.h"
#include "PINDEFS.h"
#include "InitializeHardware.h"
#include "Game.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static A2State_t CurrentState;

bool A2CheckForHit(void);

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
static uint8_t LastIRState;
static uint16_t timeStart;
static uint8_t numRiseEdges = 0;
static bool isFirstEdge = true;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitA2(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = A2_Init;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostA2(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunA2(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
  switch (CurrentState)
  {
    case A2_Init:        // If current state is initial Psedudo State
    {
      if(ThisEvent.EventType == ES_INIT) {
        // change states
        CurrentState = A2_Waiting2Spawn;
        LastIRState = ReadInput(PORT_ALIEN_2_IR, BIT4HI);
      }
    }
    break;
    
    case A2_Waiting2Spawn:
    {
      if(ThisEvent.EventType == ES_A2_SPAWN) {
        printf("\r\nA2 Spawn received");
        ServoWrite(ALIEN_2_ANGLE_DOWN, ALIEN_2_SERVO_CHANNEL);
        
        // restart the respawn timer
        ES_Timer_InitTimer(A2_TIMER, RandomTime()); 
        
        /******************************************
        // TODO: ADD LOGIC FOR RANDOM TIME
        ******************************************/
        
        // change states
        CurrentState = A2_Down;
      }
    }
    break;
    
    case A2_Down:
    {
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == A2_TIMER) {
        ServoWrite(ALIEN_2_ANGLE_UP, ALIEN_2_SERVO_CHANNEL);
        CurrentState = A2_Up;
      } else if(ThisEvent.EventType == ES_GAME_OVER) {
        CurrentState = A2_Waiting2Spawn;
      }
    }
    break;
    
    case A2_Up:
    {
      if(ThisEvent.EventType == ES_A2_SHOT) {
        // move alien down
        ServoWrite(ALIEN_2_ANGLE_DOWN, ALIEN_2_SERVO_CHANNEL);
        UpdateScore();
        
        // restart the respawn timer
        // want to add some logic to change the respawn timer
        ES_Timer_InitTimer(A2_TIMER, RandomTime());
        
        // change the state
        CurrentState = A2_Down;
      } else if(ThisEvent.EventType == ES_GAME_OVER) {
        // set alien down
        ServoWrite(ALIEN_2_ANGLE_DOWN, ALIEN_2_SERVO_CHANNEL);
        CurrentState = A2_Waiting2Spawn;
      }
    }
    break;
    
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
A2State_t QueryA2FSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

bool A2CheckForHit(void) {
  //printf("Checking hit\r\n");
  uint8_t CurrentIRState = ReadInput(PORT_ALIEN_2_IR, ALIEN_2_IR_HI);
  if (CurrentIRState != LastIRState) {
    LastIRState = CurrentIRState;
    if (CurrentIRState) {
      if (isFirstEdge) {
        timeStart = ES_Timer_GetTime();
        numRiseEdges++;
        isFirstEdge = false;
      } else {
        numRiseEdges++;
        uint16_t dT = ES_Timer_GetTime() - timeStart;
        if (numRiseEdges >= 10 && (dT) < 250) {
          // valid hit
          ES_Event_t Event2Post;
          Event2Post.EventType = ES_A2_SHOT;
          PostA2(Event2Post);
          //printf("\r\nAlien 1 shot");
          return true;
        }
        else if (dT > 250) {
          // didnt get enough edges in 250 ms so reset
          numRiseEdges = 0;
          isFirstEdge = true;
        }
      }
    }
  }
  return false;
}

