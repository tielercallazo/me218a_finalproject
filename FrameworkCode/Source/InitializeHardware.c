/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <float.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "PINDEFS.h"
#include "PWM16Tiva.h"
#include "ADMulti.h"
#include "InitializeHardware.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
#define NUM_SERVOS 11

/*------------------------------ Module Code ------------------------------*/
bool InitActivatePorts(void)
{
   // Initialize Hardware
  HWREG(TOGGLE_PORT) |= (ACTIVATE_PORT_A | ACTIVATE_PORT_B | ACTIVATE_PORT_C | ACTIVATE_PORT_D | ACTIVATE_PORT_E | ACTIVATE_PORT_F);
  while ((HWREG(TOGGLE_PORT) & (ACTIVATE_PORT_A | ACTIVATE_PORT_B | ACTIVATE_PORT_C | ACTIVATE_PORT_D | ACTIVATE_PORT_E | ACTIVATE_PORT_F)) != (ACTIVATE_PORT_A | ACTIVATE_PORT_B | ACTIVATE_PORT_C | ACTIVATE_PORT_D | ACTIVATE_PORT_E | ACTIVATE_PORT_F)) {
  }
  printf("Activated all ports.\r\n");
	return true;
}


bool InitBlasterHW(void)
{
  HWREG(PORT_BLASTER_TRIGGER + GPIO_O_DEN) |= BLASTER_TRIGGER_HI;
  HWREG(PORT_BLASTER_TRIGGER + GPIO_O_DIR) &= BLASTER_TRIGGER_LO;
  HWREG(PORT_BLASTER_TRIGGER + GPIO_O_PUR) |= BLASTER_TRIGGER_HI;
  HWREG(PORT_BLASTER_MOTOR + GPIO_O_DEN) |= BLASTER_MOTOR_HI;
  HWREG(PORT_BLASTER_MOTOR + GPIO_O_DIR) |= BLASTER_MOTOR_HI;
  HWREG(PORT_BLASTER_IR + GPIO_O_DEN) |= BLASTER_IR_HI;
  HWREG(PORT_BLASTER_IR + GPIO_O_DIR) |= BLASTER_IR_HI;
  printf("Blaster Hardware Initialized.\r\n");
  return true;
}

 
bool InitCoinHW(void)
{
  HWREG(PORT_COIN_IR + GPIO_O_DEN) |= COIN_IR_HI;
  HWREG(PORT_COIN_IR + GPIO_O_DIR) &= COIN_IR_LO;
  HWREG(PORT_COIN_IR + GPIO_O_PUR) |= COIN_IR_HI;
	
  printf("Coin Hardware Initialized.\r\n");
  return true;
}

bool InitCoinServo(void) {
  PWM_TIVA_SetPeriod(ALIEN_SERVO_PERIOD, COIN_SERVO_GROUP);
  return true;
}

bool InitPWMHW(void) {
  PWM_TIVA_Init(NUM_SERVOS); // Initialize 11 Servos
  printf("Initialize 11 Servos.\r\n");
  return true;
}

bool InitAlienIRHW(void) {
  HWREG(PORT_ALIEN_1_IR + GPIO_O_DEN) |= ALIEN_1_IR_HI;
  HWREG(PORT_ALIEN_1_IR + GPIO_O_DIR) &= ALIEN_1_IR_LO;
  
  HWREG(PORT_ALIEN_2_IR + GPIO_O_DEN) |= ALIEN_2_IR_HI;
  HWREG(PORT_ALIEN_2_IR + GPIO_O_DIR) &= ALIEN_2_IR_LO;
  
  HWREG(PORT_ALIEN_3_IR + GPIO_O_DEN) |= ALIEN_3_IR_HI;
  HWREG(PORT_ALIEN_3_IR + GPIO_O_DIR) &= ALIEN_3_IR_LO;
  
  HWREG(PORT_ALIEN_4_IR + GPIO_O_DEN) |= ALIEN_4_IR_HI;
  HWREG(PORT_ALIEN_4_IR + GPIO_O_DIR) &= ALIEN_4_IR_LO;
  
  HWREG(PORT_ALIEN_5_IR + GPIO_O_DEN) |= ALIEN_5_IR_HI;
  HWREG(PORT_ALIEN_5_IR + GPIO_O_DIR) &= ALIEN_5_IR_LO;
  
  HWREG(PORT_ALIEN_6_IR + GPIO_O_DEN) |= ALIEN_6_IR_HI;
  HWREG(PORT_ALIEN_6_IR + GPIO_O_DIR) &= ALIEN_6_IR_LO;
  
  printf("Aliens 1-6 HW initialized.\r\n");
  return true;
}

bool InitAlienServoHW(void) {
  // Set Alien Servo Frequency
  PWM_TIVA_SetPeriod(ALIEN_SERVO_PERIOD, ALIEN_GROUP_12);
  PWM_TIVA_SetPeriod(ALIEN_SERVO_PERIOD, ALIEN_GROUP_34);
  PWM_TIVA_SetPeriod(ALIEN_SERVO_PERIOD, ALIEN_GROUP_56);
  printf("Servo Period set to: %u\r\n", ALIEN_SERVO_PERIOD);
  return true;
}

bool InitTurretHW(void)
{
  HWREG(PORT_TURRET_TRIGGER + GPIO_O_DEN) |= TURRET_TRIGGER_HI;
  HWREG(PORT_TURRET_TRIGGER + GPIO_O_DIR) &= TURRET_TRIGGER_LO;
  HWREG(PORT_TURRET_TRIGGER + GPIO_O_PUR) |= TURRET_TRIGGER_HI;
  HWREG(PORT_TURRET_IR + GPIO_O_DEN) |= TURRET_IR_HI;
  HWREG(PORT_TURRET_IR + GPIO_O_DIR) |= TURRET_IR_HI;
  printf("Turret HW Initialized\r\n");
  return true;
}

bool InitBlasterTurretIRHW(void) {
  PWM_TIVA_SetFreq(BLASTER_TURRET_FREQ, BLASTER_TURRET_GROUP);
  PWM_TIVA_SetDuty(0, BLASTER_IR_CHANNEL);
  PWM_TIVA_SetDuty(0, TURRET_IR_CHANNEL);
  printf("Blaster and Turret IR Initialized\r\n");
  return true;
}

bool InitTurretClockServoHW(void) {
  PWM_TIVA_SetPeriod(TURRET_SERVO_PERIOD, TURRET_CLOCK_GROUP);
  printf("Turret servo Period set to: %u\r\n", TURRET_SERVO_PERIOD);
  return true;
}

bool InitTurretPOTHW(void) {
  ADC_MultiInit(1);
  return true;
}

uint8_t ReadInput(int PORT, uint8_t PINHI) {
  // printf("%X\r\n", HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT3HI);
  return HWREG(PORT + (GPIO_O_DATA + ALL_BITS)) & PINHI;
}

uint16_t GetPulseWidth(uint8_t angle_degrees) {
  uint16_t NewPW_time; // microseconds
  NewPW_time = 2450 - 11 * angle_degrees;
//  NewPW_time = 5.56 * angle_degrees + 1500;
	//printf("/r/nPW Time: %d", NewPW_time);
  return (NewPW_time) / 0.8; // 0.8 microseconds per tick
}

int16_t ConvertCountToPulseWidth(int16_t analog_count) {
  int16_t PW;
//  PW = 0.244 * analog_count + 1000;
//  PW = -0.488 * analog_count + 2450;
  if (analog_count >= 3000) {
    analog_count = 3000;
    } else if (analog_count <= 500) {
      analog_count = 500;
    }
    PW = -0.488 * analog_count + 2450;
  PW = (PW) / 0.8;
  return PW;
}

//int16_t ConvertCountToPulseWidthInverted(int16_t analog_count) {
//  int16_t PW;
//  PW = 0.244 * analog_count + 1000;
//  PW = (PW) / 0.8;
//  return PW;
// }

void InitAllHW(void){
	InitActivatePorts();
	InitBlasterHW();
	InitCoinHW();
	InitPWMHW();
	InitAlienServoHW();
	InitTurretHW();
	InitBlasterTurretIRHW();
	InitTurretClockServoHW();
	InitTurretPOTHW();
  InitCoinServo();
}

int16_t readAnalogInput(void) {
  uint32_t currentAnalogVal[1];
  
  ADC_MultiRead(currentAnalogVal);
  return currentAnalogVal[0];
}

uint16_t RandomTime(void) {
  uint16_t Range = ALIEN_SPAWN_TIME_MAX - ALIEN_SPAWN_TIME_MIN;
  // srand(ES_Timer_GetTime());
  return rand() % Range + ALIEN_SPAWN_TIME_MIN;
}

/***************************************************************************************/
