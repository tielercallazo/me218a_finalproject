/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Timers.h"
#include "MasterFSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define ACTIVATE_PORT_B BIT1HI
#define ACTIVATE_PORT_C BIT2HI
#define ACTIVATE_PORT_E BIT4HI

// these times assume a 1.000mS/tick timing
#define ONE_SEC 976
#define TWENTY_SEC (ONE_SEC * 20)
#define THIRTY_SEC (ONE_SEC * 30)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MasterState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMasterFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = Initialize;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  
  // Initialize Hardware
  HWREG(SYSCTL_RCGCGPIO) |= (ACTIVATE_PORT_B | ACTIVATE_PORT_C | ACTIVATE_PORT_B); // Activate Ports B, C, and E
  
  while((HWREG(SYSCTL_RCGCGPIO) & (ACTIVATE_PORT_B | ACTIVATE_PORT_C | ACTIVATE_PORT_B)) != (ACTIVATE_PORT_B | ACTIVATE_PORT_C | ACTIVATE_PORT_B)) { // Poll until active
  }
  
  // Port B
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT3HI | BIT4HI | BIT5HI | BIT6HI | BIT7HI); // Set pins 3, 4, 5, 6, 7 to digital
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT4HI | BIT5HI | BIT7HI); // Set pins 4, 5, 7 to output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT3LO & BIT6LO); // Set pins 3 and 6 to input
  HWREG(GPIO_PORTB_BASE + GPIO_O_PUR) |= BIT3HI; // Activate PUR on pin 3
  
  // Port C
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI; // Set pins 4 to digital
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO; // Set pins 4 to input
  HWREG(GPIO_PORTC_BASE + GPIO_O_PUR) |= BIT4HI; // Activate PUR on pin 4
  
  // Port E
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= BIT1HI; // Set pins 1 to digital
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) &= BIT0LO; // Set pins 0 to analog
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= BIT1HI; // Set pins 1 to output
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) &= BIT0LO; // Set pins 0 to input
  
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMasterFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMasterFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  MasterState_t NextState = CurrentState;

  switch (CurrentState)
  {
    case Initialize:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        NextState = Restart;
      }
    }
    break;

    case Restart:        // If current state is state one
    {
      switch (ThisEvent.EventType)
      {
        case ES_COIN_DETECTED:  //If event is event one

        {   // Execute action function for state one : event one
          ES_Timer_InitTimer(MASTER_TIMER, TWENTY_SEC);
          ES_Timer_InitTimer(IDLE_TIMER, THIRTY_SEC);
          CurrentState = Locked;  //Decide what the next state will be
        }
        break;

        // repeat cases as required for relevant events
        default:
          ;
      }  // end switch on CurrentEvent
    }
    break;
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
MasterState_t QueryTemplateFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

