/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Game.h"
#include "PINDEFS.h"
#include "InitializeHardware.h"
#include "TestCases.h"
#include "CoinService.h"
#include "Blaster.h"
#include "Turret.h"
#include "AlienFSM_1.h"
#include "AlienFSM_2.h"
#include "AlienFSM_3.h"
#include "AlienFSM_4.h"
#include "AlienFSM_5.h"
#include "AlienFSM_6.h"

// include the libraries with the writing funtions to servos!

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static GameState_t CurrentState;
//double CalculateAccuracy(void);

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

static uint8_t minAngle = 15;
static uint8_t maxAngle = 135;
static uint8_t displayCount = 0;
static uint8_t lightOn;
static bool isCoinDetected = false;

// score
static int16_t Score;
static int16_t AliensHit;
static int16_t BlasterShots;
static int16_t TurretShots;

// PROTOTYPES
void DisplayTick(void);
void ResetDisplayTick(void);
void ReturnCoin(void);
bool Check4Coin(void);
void UpdateScore(void);

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitGame(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitGameFSM;
  
  /**********************************/
  // init the different timers
  // ticks are in millisec
  ES_Timer_InitTimer(PHASE_CHANGE_1_TIMER, 20000); // 16 bit max is ~65000
  ES_Timer_InitTimer(PHASE_CHANGE_2_TIMER, 40000);
  ES_Timer_InitTimer(INACTIVITY_TIMER, 30000); 
  ES_Timer_InitTimer(DISPLAY_TIMER, 1000);

  // run the HW init functions below
  InitActivatePorts();
	InitCoinHW();
	InitPWMHW();
//  InitBlasterTurretIRHW();
  InitTurretPOTHW();
  InitCoinServo();
  
  PWM_TIVA_SetPeriod(4, BLASTER_TURRET_GROUP);
  /**********************************/
  
  lightOn = 1;
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostGame(ES_Event_t ThisEvent) {
  // refers to the 30 second timer for inactivity
  return ES_PostToService(MyPriority, ThisEvent);
}


/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunGame(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitGameFSM:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {        
        // change the state to idle
        CurrentState = Idle;
        ES_Timer_InitTimer(WELCOME_TIMER, 500);
      }
    }
    break;

    case Idle:
    {
			displayCount = 0;
      if(ThisEvent.EventType == ES_COIN_DETECTED)
      { // game has started :) 
        printf("\r\nCoin detected?");
				
				// phase 1 timer
				ES_Timer_InitTimer(PHASE_CHANGE_1_TIMER, 20000); 
				
				// display servo timer
				ES_Timer_InitTimer(DISPLAY_TIMER, 1000);
				
				// inactitivty timer
				ES_Timer_InitTimer(INACTIVITY_TIMER, 30000); 
				InitPWMHW();
        InitAlienIRHW();
        InitBlasterHW();
        InitAlienServoHW();
        InitTurretHW();
        InitBlasterTurretIRHW();
        InitTurretClockServoHW();
        
        // InitTurretPOTHW();
        
        // post event to blaster service
        PostBlaster(ThisEvent);
        PostTurret(ThisEvent);
        
        // post to aliens
        ES_Event_t Event2Post;
        Event2Post.EventType = ES_A1_SPAWN;
        PostA1(Event2Post);
        Event2Post.EventType = ES_A2_SPAWN;
        PostA2(Event2Post);
        Event2Post.EventType = ES_A3_SPAWN;
        PostA3(Event2Post);
        Event2Post.EventType = ES_A4_SPAWN;
        PostA4(Event2Post);
        Event2Post.EventType = ES_A5_SPAWN;
        PostA5(Event2Post);
        Event2Post.EventType = ES_A6_SPAWN;
        PostA6(Event2Post);
        
        Score = 0;
        AliensHit = 0;
        BlasterShots = 0;
        TurretShots = 0;
        
        CurrentState = Phase1;
      } else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == WELCOME_TIMER) {
        if (lightOn) {
          PWM_TIVA_SetDuty(50, TURRET_IR_CHANNEL);
          lightOn = 0;
        } else {
          PWM_TIVA_SetDuty(0, TURRET_IR_CHANNEL);
          lightOn = 1;
        }
        ES_Timer_InitTimer(WELCOME_TIMER, 1000);
      }
        else 
			{ // do nothing
			}
    }
    break;
      
    case Phase1:
    {
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == PHASE_CHANGE_1_TIMER)
      {
        // reset phase 2 change timer
				ES_Timer_InitTimer(PHASE_CHANGE_2_TIMER, 40000); 

        CurrentState = Phase2;
      } else if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DISPLAY_TIMER)
      {
				DisplayTick();
      } else 
			{ // do nothing
			}
    }
    break;
    
    case Phase2:
    {
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == PHASE_CHANGE_2_TIMER) {
				// Game is over
				
				// reset display servo
				ResetDisplayTick();
			
				// Post to the CoinService to return the coin
				ES_Event_t Event2Post;
				Event2Post.EventType = ES_GAME_OVER;
        ES_PostAll(Event2Post);
        
				// all aliens down
        Event2Post.EventType = ES_RETURN_COIN;
        PostCoinService(Event2Post);
				
				CurrentState = ReturningCoin;
      } else if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == INACTIVITY_TIMER) {
        // Game is over
        printf("\r\nTIMEOUT");
				
				// reset display servo
				ResetDisplayTick();
				
				// all aliens down
				ES_Event_t Event2Post;
        Event2Post.EventType = ES_GAME_OVER;
        
        ES_PostAll(Event2Post);
			
        // TODO: UPDATE COIN SERVICE TO RESPOND TO TO GAME OVER EVENT
				// Post to the CoinService to return the coin
				ThisEvent.EventType = 	ES_RETURN_COIN;
				PostCoinService(ThisEvent);

				// Move into "ReturningCoin" state
				CurrentState = ReturningCoin;        
      } else if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DISPLAY_TIMER) {
				DisplayTick();
      } else {
			}
    }
    break;
		
		case ReturningCoin:
			if(ThisEvent.EventType == ES_COIN_RETURNED) {
				CurrentState = Idle;
				isCoinDetected = false;
        ES_Timer_InitTimer(WELCOME_TIMER, 1000);
        printf("\r\n*****************************");
        printf("\r\n*****************************");
        printf("\r\n*****************************");
        printf("\r\nScore: %i", Score);
        printf("\r\nAliens Shot: %i", AliensHit);
        printf("\r\nBlaster Fired: %i times.", BlasterShots);
        printf("\r\nTurret Fired: %i times.", TurretShots);
//        printf("\r\nEfficiency: %d", CalculateAccuracy());
        printf("\r\n*****************************");
        printf("\r\n*****************************");
        printf("\r\n*****************************");
        // turn off all the input pins
        
        
        // turn off: blaster pins, turret pins
        
        
			} else {
			// printf("\n\rIn Game.c, Returning Coin state- event not handled by case/switch statement");
			}
		break;

    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
GameState_t QueryGameFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void DisplayTick(void) {
  // goal: write the servo value to be a scaled value between the min and max angles throughout 60 sec

  // map the counter to the range
  uint8_t displayAngle;
	uint8_t slope = (maxAngle - minAngle) / 60; // change 15 back to 60
  displayAngle = minAngle + slope * displayCount;
  
  // TODO: DEFINE DISPLAY_SERVO_CHANNEL
	ServoWrite(displayAngle, DISPLAY_SERVO_CHANNEL); // where can I find the channel to write for display servo
	
	// increment the counter
	displayCount++;
  
  // reset the timer
  ES_Timer_InitTimer(DISPLAY_TIMER, 1000);
}

void ResetDisplayTick(void) {
	ServoWrite(minAngle, DISPLAY_SERVO_CHANNEL);
}

bool Check4Coin(void){
	// printf("\r\nlooking for coin");
	if(!ReadInput(PORT_COIN_IR, COIN_IR_HI) && !isCoinDetected) {
		// post to coin service
		ES_Event_t Event2Post;
		Event2Post.EventType = ES_COIN_DETECTED;
		PostCoinService(Event2Post);
		PostGame(Event2Post);
		isCoinDetected = true;
		// printf("\r\nCoin Detected.");
		return true;
	} else {
		return false;
	}
}

void UpdateScore (void) {
  Score = Score + 10;
  AliensHit++;
}

void ScorePenaltyBlaster (void) {
  Score = Score - 3;
  BlasterShots++;
  printf("\r\nBlaster Fired");
}

void ScorePenaltyTurret(void) {
  Score = Score - 6;
  TurretShots++;
  printf("\r\nTurret Fired");
}

//double CalculateAccuracy(void) {
//  uint16_t denominator = BlasterShots + TurretShots;
//  if (denominator == 0) {
//    return 0;
//  }
//  else {
//    return AliensHit / denominator;
//  }

