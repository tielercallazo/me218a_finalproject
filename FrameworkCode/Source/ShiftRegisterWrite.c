/****************************************************************************
 Module
   ShiftRegisterWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to a write only shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass
 
****************************************************************************/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

// readability defines
#define DATA GPIO_PIN_0
#define DATA_HI BIT0HI
#define DATA_LO BIT0LO

#define SCLK GPIO_PIN_1
#define SCLK_HI BIT1HI
#define SCLK_LO BIT1LO

#define RCLK GPIO_PIN_2
#define RCLK_LO BIT2LO
#define RCLK_HI BIT2HI

#define GET_MSB_IN_LSB(x) ((x & 0x80)>>7)
#define ALL_BITS (0xff<<2)

// an image of the last 8 bits written to the shift register
static uint8_t LocalRegisterImage=0;

// Create your own function header comment
void SR_Init(void){

  // set up port B by enabling the peripheral clock, waiting for the 
  // peripheral to be ready and setting the direction
  // of PB0, PB1 & PB2 to output
  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI;
  
  while((HWREG(SYSCTL_RCGCGPIO) & BIT1HI) != BIT1HI) {
  }
  
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (DATA_HI | SCLK_HI | RCLK_HI);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (DATA_HI | SCLK_HI | RCLK_HI);
  
  // start with the data & sclk lines low and the RCLK line high
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= DATA_LO;
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= SCLK_LO;
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;
}

// Create your own function header comment
uint8_t SR_GetCurrentRegister(void){
  return LocalRegisterImage;
}

// Create your own function header comment
void SR_Write(uint8_t NewValue){
  uint8_t BitCounter;
  LocalRegisterImage = NewValue; // save a local copy

// lower the register clock
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= RCLK_LO;
// shift out the data while pulsing the serial clock  
  for(BitCounter = 0; BitCounter < 8; BitCounter++) {
    if(GET_MSB_IN_LSB(NewValue)) {
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= DATA_HI;
    } else {
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= 	DATA_LO;
    }
// Isolate the MSB of NewValue, put it into the LSB position and output to port
		NewValue = NewValue << 1;
// raise SCLK
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= SCLK_HI;
// lower SCLK
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= SCLK_LO;
// finish looping through bits in NewValue
  }
// raise the register clock to latch the new data
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= RCLK_LO;
}
