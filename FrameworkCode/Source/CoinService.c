/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "CoinService.h"
#include "PINDEFS.h"
#include "InitializeHardware.h"
#include "TestCases.h"
#include "Game.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitCoinService(uint8_t Priority)
{
  ES_Event_t ThisEvent;
	
  MyPriority = Priority;
  /********************************************/
//	InitCoinHW();
  /*******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    printf("Initialize Complete\r\n");
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostCoinService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunCoinService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************/
	uint8_t closeCoinServoAngle = 105;
	uint8_t openCoinServoAngle= 160;
	
  switch(ThisEvent.EventType)
	{
    case (ES_INIT):
    {
     printf("Closing door");
     PWM_TIVA_SetPulseWidth(GetPulseWidth(closeCoinServoAngle), COIN_SERVO_CHANNEL); 
    }
    break;
    
		case(ES_COIN_DETECTED):
		 {
			 PWM_TIVA_SetPulseWidth(GetPulseWidth(closeCoinServoAngle), COIN_SERVO_CHANNEL); 
		 }
		 break;
		 
		 case(ES_RETURN_COIN):
		 {
			 // Move servo to release coin 
			 PWM_TIVA_SetPulseWidth(GetPulseWidth(openCoinServoAngle), COIN_SERVO_CHANNEL);
			 
			 // Start 1s timer before moving servo back
			 ES_Timer_InitTimer(COIN_SERVO_RESET_TIMER, 3000);
		 }
		 break;
		 
		 case(ES_TIMEOUT):
		 {
			 if(ThisEvent.EventParam == COIN_SERVO_RESET_TIMER) {
				// CLOSE THE SERVO
        PWM_TIVA_SetPulseWidth(GetPulseWidth(closeCoinServoAngle), COIN_SERVO_CHANNEL);
        ES_Event_t Event2Post;
        Event2Post.EventType = ES_COIN_RETURNED;
        PostGame(Event2Post);
			 }
		 }
		 break;
		 
		 default:
       PWM_TIVA_SetPulseWidth(GetPulseWidth(closeCoinServoAngle), COIN_SERVO_CHANNEL);
			 // printf("\n\r In CoinService - event not handled by case/switch statement")
      ;
	 }
   /*******************************************/
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
