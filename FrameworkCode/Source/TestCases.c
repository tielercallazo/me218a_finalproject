/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "PINDEFS.h"
#include "InitializeHardware.h"
#include "PWM16Tiva.h"
#include "ADMulti.h"


int16_t readAnalogInput(void);

/*******************************************************************************************************/
void TestCoinDetection(void) {
  InitCoinHW();
  uint8_t start = ReadInput(PORT_COIN_IR, COIN_IR_HI);
  printf("start: %u\r\n", start);
  while (!kbhit()) {
    uint8_t curr = ReadInput(PORT_COIN_IR, COIN_IR_HI);
//    printf("curr: %u\r\n", curr);
    if (curr != start) {
      if (!ReadInput(PORT_COIN_IR, COIN_IR_HI)) {
      printf("TOT Inserted\r\n");
      }
      else {
      printf("TOT Removed\r\n");
      }
      start = curr;
    }
  }
}
/*******************************************************************************************************/
void TestBlasterTrigger(void) {
  InitBlasterHW();
  uint8_t start = ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI);
  printf("start: %u\r\n", start);
  while (!kbhit()) {
    uint8_t curr = ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI);
//    printf("curr: %u\r\n", curr);
    if (curr != start) {
      if (!ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI)) {
      printf("Trigger Pulled.\r\n");
      }
      else {
      printf("Trigger Released.\r\n");
      }
      start = curr;
    }
  }
}

void TestBlasterMotorTrigger(void) {
  InitBlasterHW();
  HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) &= BLASTER_MOTOR_LO;
  while (!kbhit()) {
    if (!ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI)) {
      HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) |= BLASTER_MOTOR_HI;
    }
    else {
      HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) &= BLASTER_MOTOR_LO;
    }
  }
}

void TestBlasterIR(void) {
  InitBlasterHW();
  InitBlasterTurretIRHW();
  while (!kbhit()) {
  PWM_TIVA_SetDuty(0, BLASTER_IR_CHANNEL);
  }
}

void TestBlasterMotorTriggerIR(void) {
  InitBlasterHW();
  InitBlasterTurretIRHW();
  HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) &= BLASTER_MOTOR_LO;
  while (!kbhit()) {
    if (!ReadInput(PORT_BLASTER_TRIGGER, BLASTER_TRIGGER_HI)) {
      HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) |= BLASTER_MOTOR_HI;
      PWM_TIVA_SetDuty(50, BLASTER_IR_CHANNEL);
    }
    else {
      HWREG(PORT_BLASTER_MOTOR + SET_ALL_BITS) &= BLASTER_MOTOR_LO;
      PWM_TIVA_SetDuty(0, BLASTER_IR_CHANNEL);
    }
  }
}

// function to turn on the blaster

/*******************************************************************************************************/

void Alien1IRTest(void) {
  InitAlienIRHW();
  while (!kbhit()) {
    if (ReadInput(PORT_ALIEN_1_IR, ALIEN_1_IR_HI)) {
      printf("HI\r\n");
    } else {
      printf("LO\r\n");
    }
  }
}
        

void ServoWrite(uint8_t AngleDeg, int Channel) {
	// convert the angle to a pulse width
	// printf("/r/nservo pulse: %f", GetPulseWidth(AngleDeg) * 0.8);
	PWM_TIVA_SetPulseWidth(GetPulseWidth(AngleDeg), Channel);
}

/*******************************************************************************************************/
bool TurretPOTReadTest(void) {
  InitTurretHW();
  InitTurretPOTHW();
  uint32_t result[1];
  printf("Init Turret POT Read Test inf");
  while (!kbhit()) {
    ADC_MultiRead(result);
    printf("%u counts\r\n", result[0]);
  }
  return true;
}

bool TurretPOTToServoTest(void) {
  InitTurretHW();
  InitTurretClockServoHW();
  InitTurretPOTHW();
  uint32_t result[1];
  while (!kbhit()) {
    ADC_MultiRead(result);
    uint16_t PW = ConvertCountToPulseWidth(result[0]);
    printf("%u\r\n", result[0]);
    PWM_TIVA_SetPulseWidth(PW, TURRET_SERVO_CHANNEL);
  }
  return true;
}

bool TurreTriggerTest(void) {
  InitTurretHW();
  uint8_t start = ReadInput(PORT_TURRET_TRIGGER, TURRET_TRIGGER_HI);
  printf("start: %u\r\n", start);
  while (!kbhit()) {
    uint8_t curr = ReadInput(PORT_TURRET_TRIGGER, TURRET_TRIGGER_HI);
//    printf("curr: %u\r\n", curr);
    if (curr != start) {
      if (!ReadInput(PORT_TURRET_TRIGGER, TURRET_TRIGGER_HI)) {
      printf("Trigger Pulled.\r\n");
        PWM_TIVA_SetDuty(50, TURRET_IR_CHANNEL);
        
      }
      else {
      printf("Trigger Released.\r\n");
        PWM_TIVA_SetDuty(0, TURRET_IR_CHANNEL);
      }
      start = curr;
    }
  }
  return true;
}

void TestClockServo(void) {
  InitTurretClockServoHW();
  while (!kbhit()) {
    PWM_TIVA_SetPulseWidth(GetPulseWidth(135), DISPLAY_SERVO_CHANNEL);
  
  }
}

void TestAllAlienServos(void) {
  InitAlienServoHW();
  while(1) {
    if (getchar() == 'u') {
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_1_ANGLE_UP), ALIEN_1_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_2_ANGLE_UP), ALIEN_2_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_3_ANGLE_UP), ALIEN_3_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_4_ANGLE_UP), ALIEN_4_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_5_ANGLE_UP), ALIEN_5_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_6_ANGLE_UP), ALIEN_6_SERVO_CHANNEL);
    } else if (getchar() == 'd') {
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_1_ANGLE_DOWN), ALIEN_1_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_2_ANGLE_DOWN), ALIEN_2_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_3_ANGLE_DOWN), ALIEN_3_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_4_ANGLE_DOWN), ALIEN_4_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_5_ANGLE_DOWN), ALIEN_5_SERVO_CHANNEL);
      PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_6_ANGLE_DOWN), ALIEN_6_SERVO_CHANNEL);
    }
  }
}

void TestRandomTime(void) {
  while (getchar() != 'q') {
    if (getchar()) {
      uint16_t t = RandomTime();
      printf("Time: %u", t);
    }
  }
}
  
/*******************************************************************************************************/
// #define TEST
#ifdef TEST
/* test Harness for testing this module */
#include "termio.h"
#include<stdlib.h>
#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

int main(void)
{
    // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();
//  InitActivatePorts();
//  InitPWMHW();
//	InitAlienServoHW();
  InitAllHW();
//  printf("Init Turret POT Read Test");
  /***************************************************************************/
//  ServoWrite(0, ALIEN_1_SERVO_CHANNEL);
//	while(!kbhit())
//	{}
//	ServoWrite(90, ALIEN_1_SERVO_CHANNEL);
//	while(getchar() != 'p') {}
//		ServoWrite(180, ALIEN_1_SERVO_CHANNEL);
//	while(1){
//    PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_ANGLE_DOWN), ALIEN_1_SERVO_CHANNEL);
//	}
//  TurretPOTToServoTest();
//  while (!getchar()) {
//  }
//  ServoWrite(100, COIN_SERVO_CHANNEL);

//  PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_1_ANGLE_DOWN), ALIEN_1_SERVO_CHANNEL);
//  PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_2_ANGLE_DOWN), ALIEN_2_SERVO_CHANNEL);
//  PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_3_ANGLE_DOWN), ALIEN_3_SERVO_CHANNEL);
//  PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_4_ANGLE_DOWN), ALIEN_4_SERVO_CHANNEL);
//  PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_5_ANGLE_DOWN), ALIEN_5_SERVO_CHANNEL);
//  PWM_TIVA_SetPulseWidth(GetPulseWidth(ALIEN_6_ANGLE_UP), ALIEN_6_SERVO_CHANNEL);
  
//  TestRandomTime();
//    TestClockServo();
//TurreTriggerTest();
  //TestAllAlienServos();
  /***************************************************************************/
  // return 1;
}
#endif

