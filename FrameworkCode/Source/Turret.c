/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Turret.h"
#include "InitializeHardware.h"
#include "PWM16Tiva.h"
#include "ADMulti.h"
#include "Game.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static TurretState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

bool CheckTurretButtonStatus(void);
static int16_t lastPot;
static void moveTurret(ES_Event_t ThisEvent);
bool CheckPotReading(void);
static uint8_t lastButton;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitTurretFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = TurretInit;
  
  // get an init value of the pot
  
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostTurret(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunTurret(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case TurretInit:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // activate turret hardware below
        CurrentState = TurretDeactive;
      } else if (ThisEvent.EventType == ES_COIN_DETECTED) {
//        lastPot = readAnalogInput();
//        lastButton = ReadInput(PORT_TURRET_TRIGGER, TURRET_TRIGGER_HI);
//        CurrentState = TurretDeactive;
      }
    }
    break;
    
    case TurretDeactive:
    {
      if(ThisEvent.EventType == ES_COIN_DETECTED) {
        // start the game!
        lastPot = readAnalogInput();
        lastButton = ReadInput(PORT_TURRET_TRIGGER, TURRET_TRIGGER_HI);
        CurrentState = TurretReady2Fire;
      }
    }
    break;
		
		case TurretReady2Fire:
		{
			if(ThisEvent.EventType == ES_TURRET_BUTTON_DOWN) {
				ES_Timer_InitTimer(TURRET_TIMER, 1000);
				PWM_TIVA_SetDuty(50, TURRET_IR_CHANNEL);
        ScorePenaltyTurret();
        ES_Timer_InitTimer(INACTIVITY_TIMER, 30000);
				CurrentState = TurretFiring;
			} else if(ThisEvent.EventType == ES_TURRET_HEADING_CHANGE) {
        moveTurret(ThisEvent);
			} else if(ThisEvent.EventType == ES_GAME_OVER) {
        CurrentState = TurretDeactive;
			}
		}
		break;
		
		case TurretFiring:
		{
			if(ThisEvent.EventType == ES_TURRET_HEADING_CHANGE){
				// update heading of turret
        moveTurret(ThisEvent);
			} else if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == TURRET_TIMER) {
        // stop pulsing
        PWM_TIVA_SetDuty(0, TURRET_IR_CHANNEL);
        
        // change states
        CurrentState = TurretCoolDown;
			} else if (ThisEvent.EventType == ES_GAME_OVER) {
        // stop firing and change state
        PWM_TIVA_SetDuty(0, TURRET_IR_CHANNEL);
        CurrentState = TurretDeactive;
			}
		}
		break;
		
		case TurretCoolDown:
		{
			if(ThisEvent.EventType == ES_TURRET_BUTTON_UP) {
				// restart debounce timer
        ES_Timer_InitTimer(TURRET_DEBOUNCE_TIMER, 50);
        
        CurrentState = TurretDebounce;
			} else if(ThisEvent.EventType == ES_TURRET_HEADING_CHANGE) {
				moveTurret(ThisEvent);
			} else if(ThisEvent.EventType == ES_GAME_OVER) {
        // change state
        CurrentState = TurretDeactive;
			}
		}
		break;
		
		case TurretDebounce:
		{
			if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == TURRET_DEBOUNCE_TIMER) {
        CurrentState = TurretReady2Fire;
			} else if(ThisEvent.EventType == ES_TURRET_HEADING_CHANGE) {
				moveTurret(ThisEvent);
			} else if(ThisEvent.EventType == ES_GAME_OVER) {
        CurrentState = TurretDeactive;
			}
		}
		break;
		
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
TurretState_t QueryTurretFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
bool CheckPotReading(void){
  // compare current pot to last the pot value
  // post event and update the servo reading if there is a sig change
  int16_t currentPot = readAnalogInput();
  
  if(abs(lastPot - currentPot) > 60) {
    // create new event and update the servo heading
    // printf("\r\npot val: %d", currentPot);
    ES_Event_t Event2Post;
    Event2Post.EventType = ES_TURRET_HEADING_CHANGE;
    Event2Post.EventParam = currentPot;
    PostTurret(Event2Post);
    lastPot = currentPot;
    return true;
  }
  return false;
}

bool CheckTurretButtonStatus(void) {
  // read the current button
  static uint8_t currentButton;
  currentButton = ReadInput(PORT_TURRET_TRIGGER, TURRET_TRIGGER_HI);
  
  if(lastButton != currentButton) {
    if(currentButton == 0) {
      // button low
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_TURRET_BUTTON_DOWN;
      ThisEvent.EventParam = 1;
//      printf("\n\rTurret butt down");
      PostTurret(ThisEvent);
    } else {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_TURRET_BUTTON_UP;
      ThisEvent.EventParam = 1;
//      printf("\n\rTurret butt up");
      PostTurret(ThisEvent);
    }
    lastButton = currentButton;
    return true;
  }
  return false;
}

void ReadTurretPot(void) {
  uint32_t result[1];
  ADC_MultiRead(result);
  if(abs(lastPot - result[0]) <= 30) {
    // turret heading changed
  }
}

static void moveTurret(ES_Event_t ThisEvent) {
  int16_t analogVal = ThisEvent.EventParam;
  int16_t PW = ConvertCountToPulseWidth(analogVal);
  // printf("moving turret");
  PWM_TIVA_SetPulseWidth(PW, TURRET_SERVO_CHANNEL); // writing a new angle to the turret
  // reset the inactivity timer
  // ES_Timer_InitTimer(INACTIVITY_TIMER, 30000);
}

