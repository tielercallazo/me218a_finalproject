/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef CoinService_H
#define CoinService_H

#include "ES_Types.h"
#include "ES_Events.h"

// Public Function Prototypes

bool InitCoinService(uint8_t Priority);
bool PostCoinService(ES_Event_t ThisEvent);
ES_Event_t RunCoinService(ES_Event_t ThisEvent);

#endif /* ServTemplate_H */

