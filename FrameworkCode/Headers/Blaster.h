/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef Blaster_H
#define Blaster_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Framework.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  BlasterInit, BlasterDeactive, BlasterReady2Fire, BlasterFiring, BlasterCoolDown, 
	BlasterDebounce
} BlasterState_t;

// Public Function Prototypes

bool InitBlasterFSM(uint8_t Priority);
bool PostBlaster(ES_Event_t ThisEvent);
ES_Event_t RunBlaster(ES_Event_t ThisEvent);
BlasterState_t QueryBlasterSM(void);
bool CheckBlasterButtonStatus(void);

#endif /* FSMTemplate_H */

