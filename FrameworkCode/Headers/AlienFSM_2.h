/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef AlienFSM2_H
#define AlienFSM2_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  A2_Init, A2_Waiting2Spawn,
  A2_Up, A2_Down
} A2State_t;

// Public Function Prototypes

bool InitA2(uint8_t Priority);
bool PostA2(ES_Event_t ThisEvent);
ES_Event_t RunA2(ES_Event_t ThisEvent);
A2State_t QueryA2SM(void);
bool A2CheckForHit(void);
#endif /* FSMTemplate_H */

