/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef AlienFSM3_H
#define AlienFSM3_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  A3_Init, A3_Waiting2Spawn,
  A3_Up, A3_Down
} A3State_t;

// Public Function Prototypes

bool InitA3(uint8_t Priority);
bool PostA3(ES_Event_t ThisEvent);
ES_Event_t RunA3(ES_Event_t ThisEvent);
A3State_t QueryA3SM(void);
bool A3CheckForHit(void);
#endif /* FSMTemplate_H */

