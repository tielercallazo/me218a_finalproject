/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef AlienFSM1_H
#define AlienFSM1_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  A1_Init, A1_Waiting2Spawn,
  A1_Up, A1_Down
} A1State_t;

// Public Function Prototypes

bool InitA1(uint8_t Priority);
bool PostA1(ES_Event_t ThisEvent);
ES_Event_t RunA1(ES_Event_t ThisEvent);
A1State_t QueryA1SM(void);
bool A1CheckForHit(void);
#endif /* FSMTemplate_H */

