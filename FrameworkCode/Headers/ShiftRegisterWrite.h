// create your own header file comment block
// and protect against multiple inclusions
// the common headers for C99 types 
#include <stdint.h>
#ifndef ShiftRegisterWrite_H
#define ShiftRegisterWrite_H
void SR_Init(void);
uint8_t SR_GetCurrentRegister(void);
void SR_Write(uint8_t NewValue);
#endif
