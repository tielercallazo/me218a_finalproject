/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef Turret_H
#define Turret_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Framework.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  TurretInit, TurretDeactive, TurretReady2Fire, TurretFiring, 
	TurretCoolDown, TurretDebounce
} TurretState_t;

// Public Function Prototypes

bool InitTurretFSM(uint8_t Priority);
bool PostTurret(ES_Event_t ThisEvent);
ES_Event_t RunTurret(ES_Event_t ThisEvent);
TurretState_t QueryTurretSM(void);
bool CheckTurretButtonStatus(void);
bool CheckPotReading(void);

#endif /* FSMTemplate_H */

