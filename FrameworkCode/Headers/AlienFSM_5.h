/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef AlienFSM5_H
#define AlienFSM5_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  A5_Init, A5_Waiting2Spawn,
  A5_Up, A5_Down
} A5State_t;

// Public Function Prototypes

bool InitA5(uint8_t Priority);
bool PostA5(ES_Event_t ThisEvent);
ES_Event_t RunA5(ES_Event_t ThisEvent);
A5State_t QueryA5SM(void);
bool A5CheckForHit(void);
#endif /* FSMTemplate_H */

