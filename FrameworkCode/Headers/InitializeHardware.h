#ifndef INITIALIZEHARDWARE_H
#define INITIALIZEHARDWARE_H

#include "ES_Types.h"
#include "ES_Events.h"
#include "PINDEFS.h"
#include "PWM16Tiva.h"

bool InitActivatePorts(void);
bool InitBlasterHW(void);
bool InitCoinHW(void);
bool InitPWMHW(void);
bool InitAlienServoHW(void);
bool InitTurretHW(void);
bool InitBlasterTurretIRHW(void);
bool InitTurretClockServoHW(void);
bool InitTurretPOTHW(void);
void InitAllHW(void);
uint8_t ReadInput(int PORT, uint8_t PINHI);
uint16_t GetPulseWidth(uint8_t angle_degrees);
int16_t ConvertCountToPulseWidth(int16_t analog_count);
bool InitAlienIRHW(void);
bool InitCoinServo(void);

int16_t readAnalogInput(void);
uint16_t RandomTime(void);
#endif
