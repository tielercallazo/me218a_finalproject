/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef AlienFSM4_H
#define AlienFSM4_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  A4_Init, A4_Waiting2Spawn,
  A4_Up, A4_Down
} A4State_t;

// Public Function Prototypes

bool InitA4(uint8_t Priority);
bool PostA4(ES_Event_t ThisEvent);
ES_Event_t RunA4(ES_Event_t ThisEvent);
A4State_t QueryA4SM(void);
bool A4CheckForHit(void);
#endif /* FSMTemplate_H */

