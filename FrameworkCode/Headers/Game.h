/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef Game_H
#define Game_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "PINDEFS.h"
#include "ES_Framework.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitGameFSM, Idle, Phase1, Phase2, ReturningCoin
} GameState_t;

// Public Function Prototypes

bool InitGame(uint8_t Priority);
bool PostGame(ES_Event_t ThisEvent);
bool PostCoinService(ES_Event_t ThisEvent);
ES_Event_t RunGame(ES_Event_t ThisEvent);
GameState_t QueryGameFSM(void);
bool Check4Coin(void);
void UpdateScore(void);
void ScorePenaltyBlaster (void);
void ScorePenaltyTurret(void);

ES_Event_t RunLCDService( ES_Event_t ThisEvent );

#endif /* FSMTemplate_H */

