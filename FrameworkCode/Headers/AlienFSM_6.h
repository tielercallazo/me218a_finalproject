/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef AlienFSM6_H
#define AlienFSM6_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  A6_Init, A6_Waiting2Spawn,
  A6_Up, A6_Down
} A6State_t;

// Public Function Prototypes

bool InitA6(uint8_t Priority);
bool PostA6(ES_Event_t ThisEvent);
ES_Event_t RunA6(ES_Event_t ThisEvent);
A6State_t QueryA6SM(void);
bool A6CheckForHit(void);
#endif /* FSMTemplate_H */

