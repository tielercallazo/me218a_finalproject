/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef TestCases_H
#define TestCases_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
void TestCoinDetection(void);
void TestBlasterTrigger(void);
void TestBlasterMotorTrigger(void);
void TestBlasterIR(void);
void TestBlasterMotorTriggerIR(void);
bool Alien1ServoTest(void);
void ServoWrite(uint8_t AngleDeg, int Channel);
bool TurretPOTReadTest(void);

#endif /* FSMTemplate_H */

